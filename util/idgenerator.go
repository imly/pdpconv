package util

import (
	"github.com/bwmarrin/snowflake"
	"log"
)

var node, err = snowflake.NewNode(1)

func InitNodeId(nodeId int64){
	// Create a new Node with a Node number of 1
	node, err = snowflake.NewNode(nodeId)
	if err != nil {
		log.Panic(err.Error())
		return
	}
}

func FetchID() int64{

	if err != nil {
		log.Panic(err.Error())
		return 0
	}
	// Generate a snowflake ID.
	id := node.Generate()

	// Print out the ID in a few different ways.
	log.Printf("Int64  ID: %d\n", id)
	return id.Int64()

}