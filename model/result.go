package model

type code int

const (
	CODE_SUCC code = iota
	CODE_FAILED
	CODE_LOGIN_FAILED
	CODE_FILE_MAX_FAILED
)

type Result struct {
	Status code `json:"status"`
	Message string `json:"message"`
	Data interface{} `json:"data"`
}

func NewSuccResult() Result{
	return Result{
		Status:CODE_SUCC,
	}
}

func NewSuccResult1(message string) Result{
	return Result{
		Status:CODE_SUCC,
		Message:message,
	}
}

func NewSuccResult2(data interface{}) Result{
	return Result{
		Status:CODE_SUCC,
		Data:data,
	}
}

func NewFailedResult() Result{
	return Result{
		Status:CODE_FAILED,
	}
}

func NewFailedResult1(message string) Result{
	return Result{
		Status:CODE_FAILED,
		Message:message,
	}
}

func NewFailedResult2(status code,message string) Result{
	return Result{
		Status:status,
		Message:message,
	}
}