package model

import (
	"testing"
)
import . "pdpconv/database"

func init(){
	DB.DropTableIfExists(&Document{})
	DB.DropTableIfExists(&Image{})
	DB.CreateTable(&Document{})
	DB.CreateTable(&Image{})
}

func TestSaveDoc(t *testing.T) {

	doc := Document{
		Name:"2.pdf",
		Size : 200,
		PageSize: 10,
		FilePath: "/data",
		FileSuffix:"pdf",
		Tag:"002",
		Status:TRANS_INIT,
	}
	//SaveDoc(&doc)
	if err:=SaveDoc(&doc);err == nil {
		t.Logf("id:[%v], name:[%v], size:[%v], images:[%v]",doc.ID,doc.Name,doc.Size,doc.Images)
	}else {
		t.Error(err)
	}


}

func TestSelectDocsByTag(t *testing.T) {

	docs := make([]Document,0)

	SelectDocsByTag("002",&docs)

	for _,doc := range docs {
		t.Logf("id:[%v], name:[%v], size:[%v], images:[%v]",doc.ID,doc.Name,doc.Size,doc.Images)
	}
}

func TestFindPageList(t *testing.T) {
	docs := make([]Document,0)

	FindPageList(Document{Name:"",FilePath:""},1,20,&docs)

	for _,doc := range docs {
		t.Logf("id:[%v], name:[%v], size:[%v], images:[%v]",doc.ID,doc.Name,doc.Size,doc.Images)
	}
}

func TestFindImages(t *testing.T) {
	images := make([]Image,0)
	FindImages(16,&images)

	for _,image := range images {
		t.Logf("id:[%v], name:[%v], size:[%v], docId:[%v]",image.ID,image.Name,image.Size,image.DocId)
	}
}



func TestUpload(t *testing.T){


	t.Run("upload-A",func(t *testing.T){
		doc := Document{
			Name:"3.pdf",
			Size : 200,
			PageSize: 10,
			FilePath: "/data",
			FileSuffix:"pdf",
			Tag:"003",
			Status:TRANS_INIT,
		}
		SaveDoc(&doc)
		var image1 = Image{
			Name:"1.png",
			Size:111,
			FilePath:"/data",
			FileSuffix:"xxxxxxx",
			DocId: doc.ID,
		}
		var image2 = Image{
			Name:"2.png",
			Size:222,
			FilePath:"/data",
			FileSuffix:"xxxxxxx",
			DocId: doc.ID,
		}

		SaveImage(&image1)
		SaveImage(&image2)
	})



	t.Run("show",func(t *testing.T){
		docs := make([]Document,0)

		FindPageList(Document{Name:"",FilePath:""},1,20,&docs)

		for _,doc := range docs {
			t.Logf("DOC id:[%v], name:[%v], size:[%v], images:[%v]",doc.ID,doc.Name,doc.Size,doc.Images)

			images := make([]Image,0)
			FindImages(doc.ID,&images)

			for _,image := range images {
				t.Logf("IMAGE id:[%v], name:[%v], size:[%v], docId:[%v]",image.ID,image.Name,image.Size,image.DocId)
			}
		}


	})

}