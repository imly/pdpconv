# pdpconv

#### 介绍
简单的文档转换图片服务，将pdf,doc,ppt,excel转成图片。
转换工具使用：IMagick 和 libreoffice api

#### 运行环境
* ubuntu 18.4
* 系统默认安装imagemagic-6.9 [其他版本(可选择源码安装)](https://imagemagick.org/script/download.php)
* 需要安装gcc, make, git
* 安装Ghostscript(解析pdf使用) [下载地址(源码安装)](https://github.com/ArtifexSoftware/ghostpdl-downloads/releases)
* 安装pkg-config(api使用)


    *imagemagic和ghostscript配置环境变量*


#### 安装教程

1. 安装GS

* 下载源码包，解压安装

* 创建目录
    > mkdir /usr/local/ghostscript

* 配置信息

    > configure --prefix=/usr/local/ghostscript

* 编译并安装

    > make && make install

* 配置环境变量

    > export PATH=$PATH:/usr/local/ghostscript/bin

* 测试一下

    >convert xxx.pdf  xxx.png


2. 安装ImageMagick

* 下载二进制包安装
* 可能会出现缺少依赖包，自行下载安装依赖包
* 转换pdf时可能出现没有对应的图片或者无法识别pdf，修改/etc/imagemagick-6/policy.xml
```
修改
<policy domain="coder" rights="none" pattern="PDF" />
为
<policy domain="coder" rights="read|write" pattern="PDF" />
,
添加一行
<policy domain="coder" rights="read|write" pattern="LABEL" />
```
3. 安装api工具
* https://github.com/gographics/imagick

#### 使用说明

1. 提供通用api接口(TODO)
2. 提供后台管理界面
3. 默认使用sqlite，需要自己修改数据库

#### 参与贡献

1. knight.lu

