module pdpconv

go 1.12

require (
	github.com/bwmarrin/snowflake v0.3.0
	github.com/gin-gonic/gin v1.4.0
	github.com/jinzhu/gorm v1.9.10
	github.com/kr/pretty v0.1.0 // indirect
	google.golang.org/appengine v1.6.1 // indirect
	gopkg.in/gographics/imagick.v3 v3.2.0
	gopkg.in/yaml.v2 v2.2.2
)
