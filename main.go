package main

import (
	"github.com/gin-gonic/gin"
	"io"
	"log"
	"os"
	. "pdpconv/router"
	. "pdpconv/api"
    yml "gopkg.in/yaml.v2"
	"flag"
	"strconv"
)

const (
	DEFAULT_CONF_PATH = "pdpconv.yml"
	DEFAULT_DATA_DIR = "./data"
	DEFAULT_LOG_DIR = "./log"
	DEFAULT_PORT = 9600
	DEFAULT_HOST = ""
)

type Config struct {
	Path string
	Host string `yaml:"host"`
	Port int `yaml:"port"`
	DataDir string `yaml:"dataDir"`
	LogDir string `yaml:"logDir"`
}

var ServerConf  = new(Config)

func main()  {

	configFile := flag.String("c",DEFAULT_CONF_PATH,"set configfile pdpconv.yml")
	port := flag.Int("port",DEFAULT_PORT,"set listen port")
	dataPath := flag.String("dataDir",DEFAULT_DATA_DIR,"set data restore dir")
	logPath := flag.String("logDir",DEFAULT_LOG_DIR,"set log restore dir")
	host := flag.String("host",DEFAULT_HOST,"set host")
	flag.Parse()
	if file,error := os.Open(*configFile);error != nil {
		ServerConf.Port = *port
		ServerConf.DataDir = *dataPath
		ServerConf.Host = *host
		ServerConf.LogDir = *logPath
	}else{
		yml.NewDecoder(file).Decode(ServerConf)
	}
	log.Println(ServerConf)
	DataDir = &ServerConf.DataDir

	// 禁用控制台颜色，当你将日志写入到文件的时候，你不需要控制台颜色。
	gin.DisableConsoleColor()
	// 写入日志的文件
	f, _ := os.Create(ServerConf.LogDir + "/gin.log")
	gin.DefaultWriter = io.MultiWriter(f)

	gin.SetMode(gin.ReleaseMode)

	router := InitRouter()
	router.Run(ServerConf.Host + ":"+ strconv.Itoa(ServerConf.Port))
}